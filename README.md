Alien Alias
===========
What it does
------------
This module provides a means of creating a URL alias in a Drupal site that
points to an external location (although it could be internal).

It allows the alias to be redefined which means it can be set to point to a
different location at some time, either temporarily or permanently.

The check for, and redirection of, an AlienAlias is usually performed early in
the boot-up sequence of Drupal which means it has minimal impact on resource
usage - compared to having to fully boot-up Drupal before being able to
redirect.

The specific use-case for which it was designed was in book publishing where a
link could go to a sign-up page to notify about the next book before it was
released, and then be changed to point to the actual book once published -
without having to change the content of the books themselves.

Optionally the module will keep stats on the Alien Alias requests, and a query
parameter can be defined to indicate, for example, the source of the request.

It can also passthrough any other query parameters if desired, normally it
will block them.

How it works
------------
Each AlienAlias is an entity so any number of alien aliases can be added.

When a request comes in, and before the site fully boots-up, the path is
checked to see if it's an alien alias (only a single DB check is required). If
it is, the user is immediately redirected. If not, normal site boot-up
continues.

Every AlienAlias has a Drupal 8 router entry which means Fast404 will not
claim the URL does not exist. In addition, the code adds the "alien" redirect
information into the router table, so there is no need to fetch the AlienAlias
entity itself in order to perform the jump, or record its information.

Each AlienAlias entity, individually, can have stat-keeping and fast response
turned off. (Even if they are switched on globally, as they are by default.)

The Exogen Class
----------------
This class is used to hold the Alien Alias information and is stored in the
router table. It provides a simple container for the data and does not require
accessing the, complex, entity tables to fetch.

Since it is stored in the router table it is fetched at the same time as a
match is found for the incoming URL, so there's no extra overhead to fetch the
necessary redirection data.

Global settings
---------------
There are three values that can be placed in the settings.php file:

### `$settings['alien_alias_fast_response'] = FALSE;`

Alien aliases redirections are, by default, run before full Drupal bootstrap so
that they use less memory and are dealt with quickly. This setting allows you
to switch it off and execute only through the controller after full boot up.

This overrides any individual setting alien alias setting.

### `$settings['alien_alias_keep_stats'] = FALSE;`

Also, by default, the record keeping is switched on, but can be disabled
globally in the `settings.php` file if preferred.

### `$settings['alien_alias_stat_value_regex'] = '[A-Za-z0-9\-]{32}';`

There is the possibility that certain individuals will try to fill your stats
table with garbage. To help mitigate that we use a regular expression to
validate incoming values before they can be stored. (Using standard Drupal we
prevent SQL injection anyway but keeping unnecessary stuff out of the DB is a
good thing.)

This value is the default (which also limits the length). A regex can be set
individually for any AlienAlias if desired.
