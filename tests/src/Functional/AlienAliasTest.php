<?php


namespace Drupal\Tests\alien_alias\Functional;


use Drupal\alien_alias\Entity\AlienAlias;
use Drupal\alien_alias\Entity\AlienAliasInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\BrowserTestBase;

class AlienAliasTest extends BrowserTestBase {

  /**
   * The name (url) to use for this alien alias.
   *
   * @var string
   */
  protected static $name = 'alien-test';

  /**
   * The target url to use for this alien alias.
   *
   * @var string
   */
  protected static $url = 'https://google.co.uk/';

  /**
   * The stat name to use for this alien alias.
   *
   * @var string
   */
  protected static $stat_name = 'stat_name';

  /**
   * The random default user.
   *
   * @var AccountInterface
   */
  protected $user;

  /**
   * The alien alias that we're testing here.
   *
   * @var AlienAliasInterface
   */
  protected $alien;

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  public static $modules = [
    'alien_alias',
  ];

  /**
   * The things to be setup, like a default user and a test AlienAlias.
   */
  protected function setUp() {
    parent::setUp();

    $this->user = $this->drupalCreateUser(['access content']);
    $container = \Drupal::getContainer();
    $container->get('current_user')->setAccount($this->user);

    $this->alien = AlienAlias::create([
      'name' => static::$name,
      'alien' => static::$url,
      'keep_stats' => TRUE,
      'stat' => static::$stat_name,
      'fast_reponse' => TRUE,
      'query_pass' => FALSE,
    ]);

    $this->alien->save();

    $this->container->get('router.builder')->rebuild();
  }

  /**
   * Tests the alien_alias redirect we've saved works when logged in and out.
   */
  public function testAlienAliasRedirect() {
    $account = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($account);

    $this->drupalGet(static::$name);
    $this->assertSession()->statusCodeEquals(200);

    // Test we're on the right page.
    $this->assertSession()->buttonExists("I'm Feeling Lucky");

    $this->drupalLogout();

    $this->drupalGet(static::$name);
    $this->assertSession()->statusCodeEquals(200);

    // Test we're on the right page.
    $this->assertSession()->buttonExists("I'm Feeling Lucky");
  }

}