<?php
/**
 * @file
 * Tests for the Exogen class.
 */

namespace Drupal\Tests\alien_alias\Unit;

use Drupal\alien_alias\Exogen\Exogen;
use Drupal\alien_alias\Exogen\ExogenInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Testing the Exogen class to ensure it's working.
 *
 * @group alien_alias
 */
class ExogenTest extends UnitTestCase {

  /**
   * The ID to use for the test.
   *
   * @var string
   */
  static $id = 'exogen_test';

  /**
   * The URL to use for the test.
   *
   * @var string
   */
  static $url = 'https://google.co.uk/';

  /**
   * The name of the stats flag to count to use for the test.
   *
   * @var string
   */
  static $statName = 'stat_name';

  /**
   * The exogen object being tested.
   *
   * @var ExogenInterface
   */
  protected $exogen;

  /**
   * Before a test method is run, setUp() is invoked.
   *
   * Create new Exogen object to be tested.
   */
  public function setUp() {
    $this->exogen = new Exogen(static::$id, static::$url, TRUE, static::$statName, TRUE);
  }

  /**
   * @covers \Drupal\alien_alias\Exogen\Exogen::getId
   */
  public function testGetId() {
    $this->assertEquals(static::$id, $this->exogen->getId());
  }

  /**
   * @covers \Drupal\alien_alias\Exogen\Exogen::getUrl
   */
  public function testGetUrl() {
    $this->assertEquals(static::$url, $this->exogen->getUrl());
  }

  /**
   * @covers \Drupal\alien_alias\Exogen\Exogen::isKeepStats
   */
  public function testIsPassthrough() {
    $this->exogen->setPassthrough(TRUE);
    $this->assertTrue($this->exogen->isPassthrough());
    $this->exogen->setPassthrough(FALSE);
    $this->assertFalse($this->exogen->isPassthrough());
  }

  /**
   * @covers \Drupal\alien_alias\Exogen\Exogen::isKeepStats
   */
  public function testIsKeepStats() {
    $this->exogen->setKeepStats(TRUE);
    $this->assertTrue($this->exogen->isKeepStats());
    $this->exogen->setKeepStats(FALSE);
    $this->assertFalse($this->exogen->isKeepStats());
  }

  /**
   * @covers \Drupal\alien_alias\Exogen\Exogen::getStatName
   */
  public function testGetStatName() {
    $this->exogen->setKeepStats(TRUE);
    $this->assertEquals(static::$statName, $this->exogen->getStatName());
    $this->exogen->setKeepStats(FALSE);
    $this->assertEquals('', $this->exogen->getStatName());
  }

  /**
   * Once test method has finished running, succeeded or failed, invoke this.
   *
   * Unset the $exogen object.
   */
  public function tearDown() {
    unset($this->exogen);
  }

}