<?php

/**
 * @file
 * The core testing of the AlienAlias entity.
 */

namespace Drupal\Tests\alien_alias\Kernel;


use Drupal\alien_alias\Entity\AlienAlias;
use Drupal\alien_alias\Entity\AlienAliasInterface;
use Drupal\alien_alias\Exogen\Exogen;
use Drupal\Core\Session\AccountInterface;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Testing the AlienAlias class to ensure it's working.
 *
 * @group alien_alias
 */
class AlienAliasTest extends EntityKernelTestBase {

  /**
   * The name (url) to use for this alien alias.
   *
   * @var string
   */
  protected static $name = 'alien-test';

  /**
   * The target url to use for this alien alias.
   *
   * @var string
   */
  protected static $url = 'https://google.co.uk/';

  /**
   * The stat name to use for this alien alias.
   *
   * @var string
   */
  protected static $stat_name = 'stat_name';

  /**
   * The random default user.
   *
   * @var AccountInterface
   */
  protected $user;

  /**
   * The alien alias that we're testing here.
   *
   * @var AlienAliasInterface
   */
  protected $alien;

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  public static $modules = [
    'alien_alias',
  ];

  /**
   * The things to be setup, like a default user and a test AlienAlias.
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('alien_alias');

    $this->user = $this->createUser();
    $container = \Drupal::getContainer();
    $container->get('current_user')->setAccount($this->user);

    $this->alien = AlienAlias::create([
      'name' => static::$name,
      'alien' => static::$url,
      'keep_stats' => TRUE,
      'stat' => static::$stat_name,
      'fast_response' => TRUE,
      'query_pass' => TRUE,
    ]);
  }

  /**
   * Tests basic Alien Alias entity functionality.
   */
  public function testAlienAlias() {
    $this->assertEquals(static::$name, $this->alien->getName());
    $this->assertEquals(static::$url, $this->alien->getAlienUrl());
    $this->assertEquals(static::$stat_name, $this->alien->getStatName());
  }

  /**
   * Tests basic Alien Alias entity stats functionality.
   */
  public function testAlienAliasQueryPassthrough() {
    $this->alien->setPassthrough(TRUE);
    $this->assertTrue($this->alien->isPassthrough());

    $this->alien->setPassthrough(FALSE);
    $this->assertFalse($this->alien->isPassthrough());
  }

  /**
   * Tests basic Alien Alias entity stats functionality.
   */
  public function testAlienAliasStats() {
    $this->alien->setKeepStats(TRUE);
    $this->assertTrue($this->alien->isKeepStats());
    $this->assertEquals(static::$stat_name, $this->alien->getStatName());

    $this->alien->setKeepStats(FALSE);
    $this->assertFalse($this->alien->isKeepStats());
    $this->assertEquals('', $this->alien->getStatName());
  }

  /**
   * Tests basic Alien Alias entity => Exogen functionality.
   */
  public function testAlienAliasExogen() {
    $this->alien->setKeepStats(TRUE);
    $exogen1 = Exogen::Factory($this->alien);

    $this->assertEquals(static::$name, $exogen1->getId());
    $this->assertEquals(static::$url, $exogen1->getUrl());
    $this->assertEquals(static::$stat_name, $exogen1->getStatName());

    $this->alien->setKeepStats(FALSE);
    $exogen2 = Exogen::Factory($this->alien);

    $this->assertEquals(static::$name, $exogen2->getId());
    $this->assertEquals(static::$url, $exogen2->getUrl());
    $this->assertEquals('', $exogen2->getStatName());
  }

}