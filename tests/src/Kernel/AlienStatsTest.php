<?php

/**
 * @file
 * Tests for the Stats manager class.
 */

namespace Drupal\Tests\alien_alias\Unit;

use Drupal\alien_alias\Entity\AlienAlias;
use Drupal\alien_alias\Entity\AlienAliasInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Testing the AlienAlias stats table to ensure it's working.
 *
 * @group alien_alias
 */
class AlienStatsTest extends EntityKernelTestBase {

  /**
   * The name of the table for alien_alias stats.
   *
   * @var string
   */
  protected static $statsTable = 'alien_alias_stats';

  /**
   * The name (url) to use for this alien alias.
   *
   * @var string
   */
  protected static $name = 'alien-test';

  /**
   * The target url to use for this alien alias.
   *
   * @var string
   */
  protected static $url = 'https://google.co.uk/';

  /**
   * The stat name to use for this alien alias.
   *
   * @var string
   */
  protected static $stat_name = 'stat_name';

  /**
   * The random default user.
   *
   * @var AccountInterface
   */
  protected $user;

  /**
   * The alien alias that we're testing here.
   *
   * @var AlienAliasInterface
   */
  protected $alien;

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  public static $modules = [
    'alien_alias',
  ];

  /**
   * The database connector.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The service for managing the stats table.
   *
   * @var \Drupal\alien_alias\AlienAliasStatsManagerInterface
   */
  protected $alienAliasStatsManager;

  /**
   * Before a test method is run, setUp() is invoked.
   */
  public function setUp() {
    parent::setUp();

    $this->user = $this->createUser();
    $container = \Drupal::getContainer();
    $container->get('current_user')->setAccount($this->user);

    $this->alien = AlienAlias::create([
      'name' => static::$name,
      'alien' => static::$url,
      'keep_stats' => TRUE,
      'stat' => static::$stat_name,
      'fast_response' => TRUE,
      'query_pass' => FALSE,
    ]);

    $this->installSchema('alien_alias', 'alien_alias_stats');

    $container = \Drupal::getContainer();
    $this->database = $container->get('database');
    $this->alienAliasStatsManager = $container->get('alien_alias.stats');
  }

  /**
   * Let's find out if there is a table at all.
   */
  public function testTableExists() {
    $schema = $this->database->schema();

    $tableExists = $schema->tableExists(static::$statsTable);
    $this->assertTrue($tableExists, sprintf('Table %s exists.', static::$statsTable));

    foreach (['id', 'aaid', 'value', 'ip_addr', 'created'] as $field) {
      $this->assertTrue($schema->fieldExists(static::$statsTable, $field), sprintf('Field %s in table %s exists.', $field, static::$statsTable));
    }
  }

  /**
   * Try creating an entry and then recovering it.
   */
  public function testAnEntry() {
    $id = $this->alienAliasStatsManager->add(999, 'a');
    $this->assertTrue($id > 0);

    $results = $this->executeQuery(['id' => $id]);

    $this->assertTrue(count($results) == 1);

    if (count($results) == 1) {
      $result = reset($results);
      $this->assertEquals(999, $result->aaid);
      $this->assertEquals('a', $result->value);
    }

    $this->alienAliasStatsManager->delete(['id' => $id]);

    $results = $this->executeQuery(['id' => $id]);

    $this->assertEmpty($results);
  }

  /**
   * Create a bunch of entries and recover them in different ways.
   */
  public function testSomeEntries() {
    $aaid = 998;
    $value = 'a';
    $timebase = 1560000000;
    $time_int = 3600;
    $count = 20;
    for ($i=0;$i<$count;++$i) {
      $this->alienAliasStatsManager->add($aaid, $value, '', $timebase + ($time_int * $i));
      $value = $value == 'a' ? 'b' : 'a';
    }

    // Count all the recorded stats...
    $results = $this->executeQuery();
    $this->assertEquals(count($results), $count, sprintf('The number of stats counted is %s (%s)', count($results), $count));

    // Count all the 'a's...
    $results = $this->executeQuery(['value' => 'a']);
    $this->assertEquals(count($results), $count / 2, sprintf('The number of "a" stats counted is %s (%s)', count($results), $count / 2));

    // Count all the 'b's...
    $results = $this->executeQuery(['value' => 'b']);
    $this->assertEquals(count($results), $count / 2, sprintf('The number of "b" stats counted is %s (%s)', count($results), $count / 2));

    // Count everything after the middle...
    $middle = $timebase + ($time_int * $count / 2) - ($time_int / 2);
    $results = $this->executeQuery([], $middle);
    $this->assertEquals(count($results), $count / 2, sprintf('The number of stats after middle is %s (%s)', count($results), $count / 2));

    // Count everything before the middle...
    $results = $this->executeQuery([], -1, $middle);
    $this->assertEquals(count($results), $count / 2, sprintf('The number of stats before middle is %s (%s)', count($results), $count / 2));

    // Count how many between...
    $start = $timebase + ($time_int * 2) - ($time_int / 2);
    $end = $timebase + ($time_int * ($count - 2)) - ($time_int / 2);
    $results = $this->executeQuery([], $start, $end);
    $this->assertEquals(count($results), $count - 4, sprintf('The number of stats between is %s (%s)', count($results), $count / 2));

    // Delete them all in one go.
    $this->alienAliasStatsManager->delete(['aaid' => $aaid]);

    $results = $this->executeQuery();
    $this->assertEmpty($results);
  }

  /**
   * Build and execute a select query, returning the results.
   *
   * @param array $conditions
   *   The field conditions.
   * @param int $start
   *   The created start condition (-1 = none).
   * @param int $end
   *   The created end condition (-1 = none).
   *
   * @return \stdClass[]
   *   The results array.
   */
  protected function executeQuery(array $conditions = [], $start = -1, $end = -1): array {
    return $this->alienAliasStatsManager
      ->getQuery($conditions, $start, $end)->execute()->fetchAll();
  }

  /**
   * Once test method has finished running, succeeded or failed, invoke this.
   */
  public function tearDown() {
    parent::tearDown();
  }

}
