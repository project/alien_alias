<?php

namespace Drupal\alien_alias;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Class AlienAliasStatsManager.
 */
class AlienAliasStatsManager implements AlienAliasStatsManagerInterface {

  const DB_NAME = 'alien_alias_stats';

  /**
   * The database handler.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a new AlienAliasStatsManager object.
   *
   * @param Connection $database
   *   The database handler.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * Creates a new table entry with these values.
   *
   * Timestamp is automatically added as 'now'.
   *
   * @param int $aaid
   *   The ID of the alien alias.
   * @param string $value
   *   The value being stored.
   * @param string $ip
   *   The IP address of the requesting site.
   * @param int $timestamp
   *   Timestamp value if we want to set it (mostly for testing).
   *
   * @return int
   *   The serial ID of the created record.
   *
   * @throws \Exception
   */
  public function add(int $aaid, string $value, string $ip = '', int $timestamp = 0): int {
    return $this->database
      ->insert(static::DB_NAME)
      ->fields([
        'aaid' => $aaid,
        'value' => $value,
        'ip_addr' => $ip,
        'created' => $timestamp ?: time(),
      ])
      ->execute();
  }

  /**
   * Returns a query set-up with the supplied values.
   *
   * We return a query so the calling code can plug-in limit values
   * or any other conditions.
   *
   * @param array $conditions
   *   Indexed by field, value is the condition value.
   * @param int $start
   *   The timestamp start.
   * @param int $end
   *   The timestamp end.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   *   The query for finding the matching records.
   */
  public function getQuery(array $conditions = [], $start = -1, $end = -1): SelectInterface {
    $query = $this->database
      ->select(static::DB_NAME, 'x')->fields('x');
    $this->conditions($query, $conditions, $start, $end);

    return $query;
  }

  /**
   * Add the conditions to the query.
   *
   * Array values will use "IN".
   *
   * If a string contains '%' then 'LIKE' will be used instead of '='.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface|\Drupal\Core\Database\Query\SelectInterface|\Drupal\Core\Database\Query\Delete $query
   *   The query.
   * @param array $conditions
   *   Indexed by field, value is the condition value.
   * @param int $start
   *   The timestamp start.
   * @param int $end
   *   The timestamp end.
   */
  protected function conditions($query, array $conditions = [], $start = -1, $end = -1): void {
    foreach ($conditions as $field => $value) {
      if (is_array($value)) {
        $query->condition($field, $value, 'IN');
      }
      else {
        if (is_string($value)) {
          if (substr_count($value, '%')) {
            $query->condition($field, $value, 'LIKE');
          }
          else {
            $query->condition($field, $value, '=');
          }
        }
        else {
          $query->condition($field, $value, '=');
        }
      }
    }
    if ($start > -1) {
      $query->condition('created', $start, '>');
    }
    if ($end > -1) {
      $query->condition('created', $end, '<');
    }
  }

  /**
   * Deletes matching records.
   *
   * @param array $conditions
   *   Indexed by field, value is the condition value.
   * @param int $start
   *   The timestamp start.
   * @param int $end
   *   The timestamp end.
   */
  public function delete(array $conditions = [], $start = 0, $end = -1): void {
    $query = $this->database->delete(static::DB_NAME);
    $this->conditions($query, $conditions, $start, $end);
    $query->execute();
  }
}
