<?php

namespace Drupal\alien_alias\Controller;

use Drupal\alien_alias\AlienAliasAccesses;
use Drupal\alien_alias\Exogen\Exogen;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AlienAliasController.
 *
 * Controls the display of the static page.
 */
class AlienAliasController extends ControllerBase {

  /**
   * The place we're storing the static aliases.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $aliases;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Constructs a new StaticPageController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $stack
   *   The page request stack.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, RequestStack $stack) {
    $this->aliases = $entityTypeManager->getStorage('alien_alias');
    $this->request = $stack->getCurrentRequest();
  }

  /**
   * Create a new instantiation of this class.
   *
   * @param ContainerInterface $container
   *   The container we're using.
   *
   * @return ControllerBase|AlienAliasController
   *   The instantiation.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('request_stack')
    );
  }

  /**
   * Redirect to the new place.
   *
   * @param string $alien_alias_name
   *   The name of the alien alias we're redirecting to.
   *
   * @return TrustedRedirectResponse | Response
   *   Redirect to the other place (or do a fast 404).
   */
  public function redirector(string $alien_alias_name) {
    /** @var \Drupal\alien_alias\Entity\AlienAliasInterface $alien_alias */
    $alien_alias = $this->aliases->load($alien_alias_name);
    $exogen = Exogen::Factory($alien_alias);

    // If we're recording this event, record it.
    if (!AlienAliasAccesses::Record($exogen, $this->request)) {
      // But if it fails, it means the stats value was bad.
      return AlienAliasAccesses::Respond404($this->request);
    }

    // Set up the options for the aliased URL.
    $options = [
      'query' => AlienAliasAccesses::QueryParams($exogen, $this->request),
    ];

    // Return the redirect as the controlle response, nothing further
    // will be called and the redirect will be executed.
    return new TrustedRedirectResponse($exogen->getModifiedUrl($options));
  }

}
