<?php

namespace Drupal\alien_alias;

/**
 * Class AlienAliasGlobals
 *
 * @package Drupal\alien_alias
 */
final class AlienAliasGlobals {

  const FAST_RESPONSE = TRUE;

  const KEEP_STATS = TRUE;

  const STAT_VALUE_REGEX = '[A-Za-z0-9\-]{1,32}';

}
