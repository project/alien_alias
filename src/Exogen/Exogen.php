<?php


namespace Drupal\alien_alias\Exogen;

use Drupal\alien_alias\AlienAliasGlobals;
use Drupal\alien_alias\Entity\AlienAliasInterface;
use Drupal\Core\Url;

/**
 * Class Exogen
 *
 * Simple helper class to put in the routing table containing
 * the alien alias data - for simpler handling.
 *
 * @package Drupal\alien_alias\Exogen
 */
class Exogen implements ExogenInterface {

  /**
   * The ID of the alien alias we're dealing with.
   *
   * @var int
   */
  protected $id;

  /**
   * The alien alias to jump to.
   *
   * @var string
   */
  protected $url;

  /**
   * Whether this alien alias is keeping stats.
   *
   * @var bool
   */
  protected $keepStats;

  /**
   * The query argument name.
   *
   * @var string
   */
  protected $statName;

  /**
   * The regex for the query argument value.
   *
   * @var string
   */
  protected $statValueRegex;

  /**
   * Whether this alien alias passes on query parameters.
   *
   * @var bool
   */
  protected $passthrough;

  /**
   * @var bool|null
   */
  protected $fastResponse;

  /**
   * Build a new exogen object with the supplied alien alias
   *
   * @param AlienAliasInterface $alienAlias
   *   The alien alias we are simplifying.
   *
   * @return ExogenInterface
   *   The exogen object
   */
  public static function Factory(AlienAliasInterface $alienAlias): ExogenInterface {
    return new static(
      $alienAlias->id(),
      $alienAlias->getAlienUrl(),
      $alienAlias->isKeepStats(),
      $alienAlias->getStatName(),
      $alienAlias->getStatValueRegex(),
      $alienAlias->isPassthrough(),
      $alienAlias->isFastResponse()
    );
  }

  /**
   * Exogen constructor.
   *
   * @param int $id
   *   The id of the alien alias we're dealing with.
   * @param string $url
   *   The alien alias to jump to.
   * @param bool $keepStats
   *   Whether this alien alias is keeping stats.
   * @param string $statName
   *   The query argument name.
   * @param string|null $statValueRegex
   * @param bool|null $passthrough
   *   Whether this alien alias passes on query parameters.
   * @param bool|null $fastResponse
   */
  public function __construct(int $id, string $url, ?bool $keepStats, ?string $statName, ?string $statValueRegex, ?bool $passthrough, ?bool $fastResponse) {
    $this->id = $id;
    $this->url = $url;
    $this->keepStats = $keepStats ?? FALSE;
    $this->statName = $statName ?? '';
    $this->statValueRegex = $statValueRegex ?? AlienAliasGlobals::STAT_VALUE_REGEX;
    $this->passthrough = $passthrough ?? FALSE;
    $this->fastResponse = $fastResponse ?? TRUE;
  }

  /**
   * @inheritDoc
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * @inheritDoc
   */
  public function getUrl(): string {
    return $this->url;
  }

  /**
   * @inheritDoc
   */
  public function getModifiedUrl(array $options): string {
    return Url::fromUri($this->getUrl(), $options)->toString();
  }

  /**
   * @inheritDoc
   */
  public function isKeepStats(): bool {
    return $this->keepStats;
  }

  /**
   * @inheritDoc
   */
  public function setKeepStats(bool $keepStats): ExogenInterface {
    $this->keepStats = $keepStats;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getStatName(): string {
    return $this->keepStats ? $this->statName : '';
  }

  /**
   * @inheritDoc
   */
  public function isPassthrough(): bool {
    return $this->passthrough;
  }

  /**
   * @inheritDoc
   */
  public function setPassthrough(bool $passthrough): ExogenInterface {
    $this->passthrough = $passthrough;
    return $this;
  }

  /**
   * Get the alien alias stat value regex.
   *
   * @return string
   *   The alien alias stat value regex, never empty.
   */
  public function getStatValueRegex(): string {
    return $this->statValueRegex;
  }

  /**
   * Get whether this alien alias requires a fast response.
   *
   * @return bool
   *   The fast response flag.
   */
  public function isFastResponse(): bool {
    return $this->fastResponse;
  }
}
