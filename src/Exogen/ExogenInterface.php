<?php


namespace Drupal\alien_alias\Exogen;


interface ExogenInterface {

  /**
   * Get the alien alias ID.
   *
   * @return int
   *   The alien alias ID.
   */
  public function getId(): int;

  /**
   * Get the alien alias URL.
   *
   * @return string
   *   The alien alias URL.
   */
  public function getUrl(): string;

  /**
   * Get the alien alias URL, modified by the options supplied.
   *
   * @param array $options
   *   The URL options to be applied.
   *
   * @return string
   *   The alien alias URL, modified by options.
   */
  public function getModifiedUrl(array $options): string;

  /**
   * Get whether this alien alias is keeping stats.
   *
   * @return bool
   *   The keeping stats flag.
   */
  public function isKeepStats(): bool;

  /**
   * Set whether this alien alias is keeping stats.
   *
   * @param bool $keepStats
   *   The flag as to whether this alien alias is keeping stats.
   *
   * @return ExogenInterface
   *   Return self for chaining.
   */
  public function setKeepStats(bool $keepStats): ExogenInterface;

  /**
   * Get the alien alias stat argument name (if any).
   *
   * @return string
   *   The alien alias stat argument name (or empty string)
   */
  public function getStatName(): string;

  /**
   * Get the alien alias stat value regex.
   *
   * @return string
   *   The alien alias stat value regex, never empty.
   */
  public function getStatValueRegex(): string;

  /**
   * Get whether this alien alias passes through query parameters.
   *
   * @return bool
   *   The passthrough flag.
   */
  public function isPassthrough(): bool;

  /**
   * Set whether this alien alias passes through query parameters.
   *
   * @param bool $passthrough
   *   The flag as to whether this alien alias passes through query parameters.
   *
   * @return ExogenInterface
   *   Return self for chaining.
   */
  public function setPassthrough(bool $passthrough): ExogenInterface;

  /**
   * Get whether this alien alias requires a fast response.
   *
   * @return bool
   *   The fast response flag.
   */
  public function isFastResponse(): bool;

}
