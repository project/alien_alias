<?php

namespace Drupal\alien_alias;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Alien alias entity.
 *
 * @see \Drupal\alien_alias\Entity\AlienAlias.
 */
class AlienAliasAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\alien_alias\Entity\AlienAliasInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished alien alias entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published alien alias entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit alien alias entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete alien alias entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add alien alias entities');
  }

}
