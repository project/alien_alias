<?php

namespace Drupal\alien_alias\Routing;

use Drupal\alien_alias\Exogen\Exogen;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class StaticPageRouting.
 *
 * Provides the individual routing for each static page.
 */
class AlienAliasRouting implements ContainerInjectionInterface {

  /**
   * The storage for the static aliases.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $aliases;

  /**
   * AlienAliasRouting constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager we're using.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->aliases = $entityTypeManager->getStorage('alien_alias');
  }

  /**
   * Instantiates a new instance of this class.
   *
   * This is a factory method that returns a new instance of this class. The
   * factory should pass any needed dependencies into the constructor of this
   * class, but not the container itself. Every call to this method must return
   * a new instance of this class; that is, it may not implement a singleton.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container this instance should use.
   *
   * @return static
   *   The new instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function routes() {
    $collection = new RouteCollection();

    /** @var \Drupal\alien_alias\Entity\AlienAliasInterface $alien */
    foreach ($this->aliases->loadMultiple() as $alien) {
      if (!$alien->isPublished()) {
        // Don't include disabled AlienAliases.
        continue;
      }

      $route = new Route(
        $alien->getName(),

        // Route defaults:
        [
          '_controller' => '\Drupal\alien_alias\Controller\AlienAliasController::redirector',
          // We can force a parameter even though there isn't one in the URL,
          // the controller itself probably won't be used.
          'alien_alias_name' => $alien->id(),
        ],

        // Route requirements:
        [
          '_permission'  => 'access content',
        ],

        // Options - naughty, we add our own value which gets stored in the
        // router table, this means we can jump to the URL without messing
        // about with the controller. We use the 'Exogen' object which is a
        // simplified version of the equivalent AlienAlias.
        [
          '_exogen' => Exogen::Factory($alien),
        ]
      );

      $collection->add("alien_alias.{$alien->id()}.redirect", $route);
    }

    return $collection;
  }

}
