<?php

namespace Drupal\alien_alias\TwigExtension;


use Drupal\Core\Url;

/**
 * Class AlienAliasTwigExtension.
 */
class AlienAliasTwigExtension extends \Twig_Extension {


   /**
    * {@inheritdoc}
    */
    public function getTokenParsers() {
      return [];
    }

   /**
    * {@inheritdoc}
    */
    public function getNodeVisitors() {
      return [];
    }

   /**
    * {@inheritdoc}
    */
    public function getFilters() {
      return [];
    }

   /**
    * {@inheritdoc}
    */
    public function getTests() {
      return [];
    }

   /**
    * {@inheritdoc}
    */
    public function getFunctions() {
      return [
        new \Twig_SimpleFunction('alien_alias_url', [$this, 'alienAliasUrl']),
      ];
    }

   /**
    * {@inheritdoc}
    */
    public function getOperators() {
      return [];
    }

   /**
    * {@inheritdoc}
    */
    public function getName() {
      return 'alien_alias.twig.extension';
    }

  /**
   * Returns the complete Alien Alias URL.
   *
   * @param string $id
   *   The ID of the alien alias we're working with.
   * @param string $keyword
   *   If the
   *
   * @return string
   *   The URL of the alien alias, perhaps with a query parameter.
   */
    public function alienAliasUrl(string $id, string $keyword = ''): string {
      $options = [
        'absolute' => TRUE,
        'https' => TRUE,
        'query' => $keyword ? [$keyword => 'TEST'] : NULL,
      ];
      return Url::fromRoute("alien_alias.{$id}.redirect", [], $options)->toString();
    }

}
