<?php

namespace Drupal\alien_alias;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Alien alias entities.
 *
 * @ingroup alien_alias
 */
class AlienAliasListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Alias');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\alien_alias\Entity\AlienAlias */
    $row['id'] = Link::fromTextAndUrl(
      $entity->id(),
      Url::fromRoute("alien_alias.{$entity->id()}.redirect", [], ['absolute' => TRUE])
    );
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.alien_alias.edit_form',
      ['alien_alias' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
