<?php

namespace Drupal\alien_alias\EventSubscriber;

use Drupal\alien_alias\AlienAliasAccesses;
use Drupal\alien_alias\Exogen\ExogenInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Site\Settings;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Route;

/**
 * Class AlienAliasSubscriber.
 */
class AlienAliasSubscriber implements EventSubscriberInterface {

  use LoggerChannelTrait;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * @var EventDispatcherInterface
   */
  protected $dispatcher;

  /**
   * Constructs a new AlienAliasSubscriber object.
   *
   * @param RequestStack $request_stack
   *   The current request stack so we can get the current request.
   * @param EventDispatcherInterface $dispatcher
   *   The event dispatcher so we can tell the world...
   */
  public function __construct(RequestStack $request_stack,
                              EventDispatcherInterface $dispatcher) {
    $this->request = $request_stack->getCurrentRequest();
    $this->dispatcher = $dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    // Execute before Fast404, though there is a
    // defined route so it would be fine anyway.
    $events[KernelEvents::REQUEST] = ['onKernelRequest', 1000];

    return $events;
  }

  /**
   * Check whether the requested path is an alien alias, if it is
   * we redirect straight to it. This avoids unnecessary boot-up.
   *
   * @param GetResponseEvent $event
   */
  public function onKernelRequest(GetResponseEvent $event) {
    // Check to see whether we want to prevent processing alien aliases here.
    if (!Settings::get('alien_alias_fast_response', TRUE)) {
      // Yes, we don't want to do it at all, just leave.
      return;
    }

    // Get the path from the request.
    $path = $this->request->getPathInfo();

    // Ignore calls to the homepage, to avoid unnecessary processing.
    if (!isset($path) || $path == '/') {
      return;
    }

    // Find exact match for the alias and, if found, return the route data.
    $sql = "SELECT route FROM {router} WHERE path = :path";
    $result = Database::getConnection()->query($sql, [':path' => $path])->fetchField();

    // Check we got a result (a string) and that it contains "_exogen".
    if (is_string($result) && strpos($result, '_exogen') !== FALSE) {

      /** @var Route $route */
      $route = unserialize($result);

      /** @var ExogenInterface $exogen */
      $exogen = $route->getOption('_exogen');

      if (!$exogen->isFastResponse()) {
        // This individual AlienAlias is not fast response.
        return;
      }

      // If we're recording this event, record it.
      if (!AlienAliasAccesses::Record($exogen, $this->request)) {
        // But if it fails, it means the stats value was bad.
        $event->setResponse(AlienAliasAccesses::Respond404($this->request));
        return;
      }

      // Set up the options for the aliased URL (if any).
      $options = [
        'query' => AlienAliasAccesses::QueryParams($exogen, $this->request),
      ];

      // Set the redirect as the main event response, nothing further
      // will be called and the redirect will be executed.
      $event->setResponse(new TrustedRedirectResponse($exogen->getModifiedUrl($options)));
    }
  }

}
