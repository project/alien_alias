<?php

namespace Drupal\alien_alias;

use Drupal\Core\Database\Query\SelectInterface;

/**
 * Interface AlienAliasStatsManagerInterface.
 */
interface AlienAliasStatsManagerInterface {

  /**
   * Creates a new table entry with these values.
   *
   * Timestamp is automatically added as 'now'.
   *
   * @param int $aaid
   *   The ID of the alien alias.
   * @param string $value
   *   The value being stored.
   * @param string $ip
   *   The IP address of the requesting site.
   * @param int $timestamp
   *   Timestamp value if we want to set it (mostly for testing).
   *
   * @return int
   *   The serial ID of the created record.
   */
  public function add(int $aaid, string $value, string $ip = '', int $timestamp = 0): int;

  /**
   * Returns a query set-up with the supplied values.
   *
   * We return a query so the calling code can plug-in limit values
   * or any other conditions.
   *
   * If a string contains '%' then 'LIKE' will be used instead of '='.
   *
   * @param array $conditions
   *   Indexed by field, value is the condition value.
   * @param int $start
   *   The timestamp start.
   * @param int $end
   *   The timestamp end.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   *   The query for finding the matching records.
   */
  public function getQuery(array $conditions = [], $start = 0, $end = -1): SelectInterface;


  /**
   * Deletes matching records.
   *
   * If a string contains '%' then 'LIKE' will be used instead of '='.
   *
   * @param array $conditions
   *   Indexed by field, value is the condition value.
   * @param int $start
   *   The timestamp start.
   * @param int $end
   *   The timestamp end.
   */
  public function delete(array $conditions = [], $start = 0, $end = -1): void;

}
