<?php


namespace Drupal\alien_alias;

use Drupal\alien_alias\Exogen\ExogenInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Database\Database;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

/**
 * Class AlienAliasAccesses
 *
 * @package Drupal\alien_alias
 */
final class AlienAliasAccesses {

  /**
   * Flags permitting, we store this information in the database.
   *
   * @param \Drupal\alien_alias\Exogen\ExogenInterface $exogen
   *   This alien alias.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current page request.
   * @param int|null $time
   *
   * @return bool
   *   False = failed.
   */
  public static function Record(ExogenInterface $exogen, Request $request, ?int $time = NULL): bool {
    $logger = \Drupal::logger('AA Access');
    $args = [
      '@i' => $exogen->getId(),
    ];
    // Are we stat-keeping at all? Is it switched on for this alien alias?
    if (Settings::get('alien_alias_keep_stats', AlienAliasGlobals::KEEP_STATS) && $exogen->isKeepStats()) {
      $statName = $exogen->getStatName();

      // Initialise the value to '[none]', in case there's no value because
      // if a stat is going to be kept at all the value must be something.
      $value = '[none]';
      $args['@v'] = $value;

      // Make sure the stat name exists as a key in the request query.
      if ($statName && $request->query->has($statName)) {
        // Okay, this should have a value.
        if ($value = $request->query->get($statName)) {
          // Now we must validate it.
          $pattern = '/' . $exogen->getStatValueRegex() . '/';
          $args['@v'] = $value;
          $args['@p'] = $pattern;

          if (!preg_match($pattern, $value)) {
            $logger->debug(new TranslatableMarkup('Stat value @v for @i invalid, does not match @p.', $args));
            // The supplied value didn't match the pattern for this AlienAlias.
            return FALSE;
          }
          else {
            $logger->debug(new TranslatableMarkup('Stat value @v for @i is valid.', $args));
          }
        }
        else {
          $logger->debug(new TranslatableMarkup('Stat value for @i is empty (using @v).', $args));
        }
      }
      else {
        $logger->debug(new TranslatableMarkup('No stat value exists for @i using @v.', $args));
      }

      // Insert the stats data into the table.
      try {
        $sql = "INSERT INTO {alien_alias_stats} (`aaid`, `value`, `ip_addr`, `created`) VALUES (:aaid, :value, :ip_addr, :created)";
        Database::getConnection()->query($sql, [
          ':aaid' => $exogen->getId(),
          ':value' => $value,
          ':ip_addr' => $request->getClientIp() ?? '',
          ':created' => $time ?: time(),
        ]);
      }
      catch (\Exception $e) {
        $args['@x'] = $e->getMessage();
        $logger->debug(new TranslatableMarkup('Failed trying to save @i using @v because: @x.', $args));
      }
    }
    else {
      $logger->debug(new TranslatableMarkup('No stats for @i', $args));
    }

    // We're good (might not have recorded anything).
    return TRUE;
  }

  /**
   * Flags permitting, return an array of the query parameters to go.
   *
   * @param ExogenInterface $exogen
   *   This alien alias.
   * @param Request $request
   *   The current page request.
   *
   * @return mixed[]
   *   An array of the query parameters to go.
   */
  public static function QueryParams(ExogenInterface $exogen, Request $request): array {
    $params = [];

    if ($exogen->isPassthrough()) {
      // Get all the query parameters.
      $params = $request->query->getIterator()->getArrayCopy();

      // Remove the stat query parameter (if any).
      if ($statName = $exogen->getStatName()) {
        unset($params[$statName]);
      }
    }

    return $params;
  }

  /**
   * Prepare a 404 response (use fast404's response if it's there).
   *
   * @param Request $request
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public static function Respond404(Request $request) {
    $message = Settings::get('fast404_html', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server (Fast 404).</p></body></html>');

    header((Settings::get('fast404_HTTP_status_method', 'mod_php') == 'FastCGI' ? 'Status:' : 'HTTP/1.0') . ' 404 Not Found');

    return new Response(new FormattableMarkup($message, ['@path' => $request->getPathInfo()]), 404);
  }
}
