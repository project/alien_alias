<?php

namespace Drupal\alien_alias\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\UserInterface;

/**
 * Defines the Alien alias entity.
 *
 * @ingroup alien_alias
 *
 * @ContentEntityType(
 *   id = "alien_alias",
 *   label = @Translation("Alien alias"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\alien_alias\AlienAliasListBuilder",
 *     "views_data" = "Drupal\alien_alias\Entity\AlienAliasViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\alien_alias\Form\AlienAliasForm",
 *       "add" = "Drupal\alien_alias\Form\AlienAliasForm",
 *       "edit" = "Drupal\alien_alias\Form\AlienAliasForm",
 *       "delete" = "Drupal\alien_alias\Form\AlienAliasDeleteForm",
 *     },
 *     "access" = "Drupal\alien_alias\AlienAliasAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\alien_alias\AlienAliasHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "alien_alias",
 *   admin_permission = "administer alien alias entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/alien_alias/{alien_alias}",
 *     "add-form" = "/admin/structure/alien_alias/add",
 *     "edit-form" = "/admin/structure/alien_alias/{alien_alias}/edit",
 *     "delete-form" = "/admin/structure/alien_alias/{alien_alias}/delete",
 *     "collection" = "/admin/structure/alien_alias",
 *   },
 *   field_ui_base_route = "alien_alias.settings"
 * )
 */
class AlienAlias extends ContentEntityBase implements AlienAliasInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAlienUrl() {
    return $this->get('alien')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAlienUrl($alien) {
    $this->set('alien', $alien);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isKeepStats() {
    return (bool) $this->get('keep_stats')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setKeepStats($keepStats) {
    $this->set('keep_stats', $keepStats ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatName() {
    return $this->isKeepStats() ? $this->get('stat')->value : '';
  }

  /**
   * {@inheritdoc}
   */
  public function setStatName($stat) {
    $this->set('stat', $stat);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatValueRegex() {
    return $this->isKeepStats() ? $this->get('regex')->value : '';
  }

  /**
   * {@inheritdoc}
   */
  public function setStatValueRegex($regex) {
    $this->set('regex', $regex);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPassthrough() {
    return (bool) $this->get('query_pass')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPassthrough($passthrough) {
    $this->set('query_pass', $passthrough ? TRUE : FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function isFastResponse() {
    return (bool) $this->get('fast_response')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setFastResponse($fastResponse) {
    $this->set('fast_response', $fastResponse ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Authored by'))
      ->setDescription(new TranslatableMarkup('The user ID of author of the Alien alias entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Alias'))
      ->setDescription(new TranslatableMarkup('The local alias which will be redirected to the (alien) URL, it can be generated automatically, or manually.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -100,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -100,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['alien'] = BaseFieldDefinition::create('uri')
      ->setLabel(new TranslatableMarkup('Alien URL'))
      ->setDescription(new TranslatableMarkup('The fully-qualified (alien) URL for which this item is an alias. (This can also be local but must be fully qualified.)'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -80,
      ])
      ->setDisplayOptions('form', [
        'type' => 'uri',
        'weight' => -80,
      ])
      ->addConstraint('ValidAlienUrl')
      ->addConstraint('UniqueAlienUrl')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['keep_stats'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Keep stats'))
      ->setDescription(new TranslatableMarkup('Uncheck this box if you do not want to keep stats for this Alien alias.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -60,
      ]);

    $fields['stat'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Stat argument'))
      ->setDescription(new TranslatableMarkup('When keeping stats you can define an (optional) argument which can be used to differentiate calls to the same alien alias.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -50,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -50,
        'settings' => [
          'placeholder' => new TranslatableMarkup('e.g. __src'),
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['regex'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Regular expression'))
      ->setDescription(new TranslatableMarkup('When keeping stats a regular expression is used to verify the supplied stat value, to mitigate hacking attempts.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -45,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -45,
        'settings' => [
          'placeholder' => new TranslatableMarkup('Will use default if not set.'),
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['query_pass'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Passthrough query parameters'))
      ->setDescription(new TranslatableMarkup('When this is checked, query parameters (except the alien alias stat parameter) will be added to the called URL.'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -30,
      ]);

    $fields['fast_response'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Fast response'))
      ->setDescription(new TranslatableMarkup('Uncheck this box if you do not want to this Alien alias to be executed as fast as possible.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -25,
      ]);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Publishing status'))
      ->setDescription(new TranslatableMarkup('A boolean indicating whether the Alien alias is published.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -20,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('The time that the entity was last edited.'));

    return $fields;
  }
}
