<?php

namespace Drupal\alien_alias\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Alien alias entities.
 *
 * @ingroup alien_alias
 */
interface AlienAliasInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Alien alias name.
   *
   * @return string
   *   Name of the Alien alias.
   */
  public function getName();

  /**
   * Sets the Alien alias name.
   *
   * @param string $name
   *   The Alien alias name.
   *
   * @return \Drupal\alien_alias\Entity\AlienAliasInterface
   *   The called Alien alias entity.
   */
  public function setName($name);

  /**
   * Gets the Alien alias creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Alien alias.
   */
  public function getCreatedTime();

  /**
   * Sets the Alien alias creation timestamp.
   *
   * @param int $timestamp
   *   The Alien alias creation timestamp.
   *
   * @return \Drupal\alien_alias\Entity\AlienAliasInterface
   *   The called Alien alias entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Alien alias published status indicator.
   *
   * Unpublished Alien alias are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Alien alias is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Alien alias.
   *
   * @param bool $published
   *   TRUE to set this Alien alias to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\alien_alias\Entity\AlienAliasInterface
   *   The called Alien alias entity.
   */
  public function setPublished($published);

  /**
   * Gets the Alien uri.
   *
   * @return string
   *   Uri of the Alien alias.
   */
  public function getAlienUrl();

  /**
   * Sets the Alien uri.
   *
   * @param string $alien
   *   The Alien uri.
   *
   * @return \Drupal\alien_alias\Entity\AlienAliasInterface
   *   The called Alien alias entity.
   */
  public function setAlienUrl($alien);

  /**
   * Returns the Alien alias keep stats indicator.
   *
   * Even if true it may be overridden by the global setting.
   *
   * @return bool
   *   TRUE if the Alien alias should keep stats.
   */
  public function isKeepStats();

  /**
   * Sets the "keep stats" flag for this specific alien alias.
   *
   * @param bool $keepStats
   *   TRUE to keep stats for this Alien alias, FALSE to not keep stats.
   *
   * @return \Drupal\alien_alias\Entity\AlienAliasInterface
   *   The called Alien alias entity.
   */
  public function setKeepStats($keepStats);

  /**
   * Gets the Alien stat argument name (if there is one).
   *
   * It also forces empty if keeping stats is off for this alien alias.
   *
   * @return string
   *   Stat argument name (if any) of the Alien alias.
   */
  public function getStatName();

  /**
   * Sets the Alien stat argument name.
   *
   * @param string $stat
   *   The Alien Stat argument name.
   *
   * @return \Drupal\alien_alias\Entity\AlienAliasInterface
   *   The called Alien alias entity.
   */
  public function setStatName($stat);

  /**
   * Gets the Alien stat value regex (if there is one).
   *
   * @return string|null
   *   Regex (if any) for this Alien alias.
   */
  public function getStatValueRegex();

  /**
   * Sets the Alien stat value regex.
   *
   * @param string $regex
   *   The Alien Stat argument name.
   *
   * @return \Drupal\alien_alias\Entity\AlienAliasInterface
   *   The called Alien alias entity.
   */
  public function setStatValueRegex($regex);

  /**
   * Returns the Alien alias "query passthrough" flag.
   *
   * This affects whether any query parameters on the alias, should be
   * passed through to the external URL or not, except the stats parameter.
   *
   * @return bool
   *   TRUE if the Alien alias should passthrough query parameters.
   */
  public function isPassthrough();

  /**
   * Sets the "query passthrough" flag for this specific alien alias.
   *
   * @param bool $passthrough
   *   TRUE to passthrough for this Alien alias, FALSE to not passthough.
   *
   * @return \Drupal\alien_alias\Entity\AlienAliasInterface
   *   The called Alien alias entity.
   */
  public function setPassthrough($passthrough);

  /**
   * Returns the Alien alias "fast response" flag.
   *
   * This affects whether the alien alias is before full Drupal bootstrap or not.
   *
   * Even if true it may be overridden by the global setting.
   *
   * @return bool
   *   TRUE if the Alien alias should keep stats.
   */
  public function isFastResponse();

  /**
   * Sets the "fast response" flag for this specific alien alias.
   *
   * @param bool $fastResponse
   *   TRUE to fast response for this Alien alias, FALSE to not fast response.
   *
   * @return \Drupal\alien_alias\Entity\AlienAliasInterface
   *   The called Alien alias entity.
   */
  public function setFastResponse($fastResponse);

}
