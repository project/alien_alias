<?php

namespace Drupal\alien_alias\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Alien alias entities.
 */
class AlienAliasViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
