<?php

namespace Drupal\alien_alias\Plugin\Validation\Constraint;

use Drupal\Core\Validation\Plugin\Validation\Constraint\UniqueFieldConstraint;

/**
 * Supports validating alien URLs.
 *
 * @Constraint(
 *   id = "UniqueAlienUrl",
 *   label = @Translation("Unique Alien URL", context = "Validation")
 * )
 */
class AlienUrlUnique extends UniqueFieldConstraint {

  public $message = 'An alias for this URL "%value" already exists. Enter a unique URL.';

}
