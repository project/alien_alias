<?php

namespace Drupal\alien_alias\Plugin\Validation\Constraint;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Url;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates alien URLs.
 *
 * Ensures the alien URL is a valid fully qualified URL.
 */
class ValidAlienUrlValidator extends ConstraintValidator {

  /**
   * Checks if the passed value is valid.
   *
   * @param FieldItemList $items
   *   The value that should be validated
   * @param Constraint $constraint
   *   The constraint for the validation
   */
  public function validate($items, Constraint $constraint) {
    foreach ($items as $item) {
      try {
        /** @var string $value */
        $value = $item->value;

        if (!UrlHelper::isValid($value, TRUE)) {
          $this->context->addViolation($constraint->notvalid, ['%uri' => $value]);
        }
        else {
          $url = Url::fromUri($value, ['absolute' => TRUE]);

          // Disallow external URLs using untrusted protocols.
          if ($url->isExternal() && !in_array(parse_url($url->getUri(), PHP_URL_SCHEME), UrlHelper::getAllowedProtocols())) {
            $this->context->addViolation($constraint->notvalid, ['%uri' => $value]);
          }
        }
      }
      catch (\InvalidArgumentException $e) {
        $this->context->addViolation($constraint->notvalid . ', because %x', ['%uri' => $value, '%x' => $e->getMessage()]);
      }
    }
  }
}
