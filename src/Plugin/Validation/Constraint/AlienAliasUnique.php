<?php

namespace Drupal\alien_alias\Plugin\Validation\Constraint;

use Drupal\Core\Validation\Plugin\Validation\Constraint\UniqueFieldConstraint;

/**
 * Supports validating alien names.
 *
 * @Constraint(
 *   id = "UniqueAlienAlias",
 *   label = @Translation("Unique Alien Alias", context = "Validation")
 * )
 */
class AlienAliasUnique extends UniqueFieldConstraint {

  public $message = 'This alias "%value" already exists. Please enter a unique alias (or generate a random one).';

}
