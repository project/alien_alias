<?php

namespace Drupal\alien_alias\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Supports validating alien URLs.
 *
 * @Constraint(
 *   id = "ValidAlienUrl",
 *   label = @Translation("Valid Alien URL", context = "Validation")
 * )
 */
class ValidAlienUrl extends Constraint {

  public $notvalid = 'This alien URL "%uri" is not valid. Please enter a correct fully qualified URL.';

}
