<?php

namespace Drupal\alien_alias\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Alien alias entities.
 *
 * @ingroup alien_alias
 */
class AlienAliasDeleteForm extends ContentEntityDeleteForm {


}
