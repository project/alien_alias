<?php

namespace Drupal\alien_alias\Form;

use Drupal\alien_alias\AlienAliasGlobals;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Alien alias edit forms.
 *
 * @ingroup alien_alias
 */
class AlienAliasForm extends ContentEntityForm {

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('alien_alias.settings');

    $defaultValue =& $form['regex']['widget'][0]['value']['#default_value'];

    $defaultValue = $defaultValue ?: ($config->get('stat_value_regex') ?? AlienAliasGlobals::STAT_VALUE_REGEX);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Alien alias.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Alien alias.', [
          '%label' => $entity->label(),
        ]));
    }

    $form_state->setRedirect('entity.alien_alias.collection');
  }

}
