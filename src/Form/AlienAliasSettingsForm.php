<?php

namespace Drupal\alien_alias\Form;

use Drupal\alien_alias\AlienAliasGlobals;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;

/**
 * Class AlienAliasSettingsForm.
 *
 * @ingroup alien_alias
 */
class AlienAliasSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'alien_alias.settings',
    ];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'alienalias_settings';
  }

  /**
   * Defines the settings form for Alien alias entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->messenger()->addWarning(
      $this->t('Alien Alias global fast response is: <strong>@onoff</strong>', [
        '@onoff' => Settings::get('alien_alias_fast_response', AlienAliasGlobals::FAST_RESPONSE) ? $this->t('On') : $this->t('Off'),
      ])
    );

    $this->messenger()->addWarning(
      $this->t('Alien Alias global "keep stats" is: <strong>@onoff</strong>', [
        '@onoff' => Settings::get('alien_alias_keep_stats', AlienAliasGlobals::KEEP_STATS) ? $this->t('On') : $this->t('Off'),
      ])
    );

    $this->messenger()->addWarning(
      $this->t('Alien Alias <em>default</em> stat value regular expression is: <strong>@regex</strong>', [
        '@regex' => Settings::get('alien_alias_stat_value_regex', AlienAliasGlobals::STAT_VALUE_REGEX),
      ])
    );

    $config = $this->config('alien_alias.settings');

    $form['stat_value_regex'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Regular expression'),
      '#description' => $this->t('Insert the regular expression to be used globally to validate the stat value by default, or leave it as it is. It can also be set on a per alien-alias basis.'),
      '#default_value' => $config->get('stat_value_regex') ?: AlienAliasGlobals::STAT_VALUE_REGEX,
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('alien_alias.settings')
      ->set('stat_value_regex', $form_state->getValue('stat_value_regex'))
      ->save();
  }

}
