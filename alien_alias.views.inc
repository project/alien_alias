<?php

/**
 * @file
 * Views integration for alien alias stats table.
 */

/**
 * Implements hook_views_data().
 */
function alien_alias_views_data() {
  $data = [];

  $table = 'alien_alias_stats';

  $data[$table]['table']['group'] = t('Alien Alias');
  $data[$table]['table']['base'] = [
    'title' => t('Alien Alias Stats'),
    'help' => t('Alien Alias Stats made visible to Views.'),
  ];

  // A simple join to the alien_alias table
  $data[$table]['table']['join'] = [
    'alien_alias' => [
      'left_field' => 'id',
      'field' => 'aaid',
    ],
  ];

  $data[$table]['aaid'] = [
    'title' => t('Alien alias'),
    'help' => t('The alien alias this stat is related to.'),

    'relationship' => [
      'base' => 'alien_alias',
      'base field' => 'id',
      'id' => 'standard',
      'label' => t('Alien alias stats'),
    ],
  ];

  // Id field.
  $data[$table]['id'] = [
    'title' => t('id'),
    'help' => t('Alien Alias stats row ID'),
    'field' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
  ];

  // Value field.
  $data[$table]['value'] = [
    'title' => t('Value'),
    'help' => t('The value of the stats keyword for the alien alias when clicked.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];

  // IP Address field.
  $data[$table]['ip_addr'] = [
    'title' => t('IP Address'),
    'help' => t('The IP address of where the alien alias was clicked'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];

  // Created timestamp field.
  $data[$table]['created'] = [
    'title' => t('Created'),
    'help' => t('The timestamp of when the alien alias was clicked.'),
    'field' => [
      'id' => 'date',
    ],
    'sort' => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
  ];


  return $data;
}
